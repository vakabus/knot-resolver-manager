# pylint: skip-file
# flake8: noqa

# throws nice syntax error on old Python versions:
0_0  # Python >= 3.6 required

from knot_resolver_manager import main
main.main()
